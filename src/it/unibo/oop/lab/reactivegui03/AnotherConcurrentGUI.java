package it.unibo.oop.lab.reactivegui03;

import it.unibo.oop.lab.reactivegui02.ConcurrentGUI;

/**
 * ConcurrentGUI auto-stopping after 10 secs.
 */
public class AnotherConcurrentGUI extends ConcurrentGUI {
    private static final long serialVersionUID = -8710276539980695794L;
    private static final long STOP_AFTER_MS = 10000;

    /**
     * Builds and starts the GUI.
     */
    public AnotherConcurrentGUI() {
        super();

        new Thread(new AutoStopAgent()).start();
    }


    private class AutoStopAgent implements Runnable {
        public void run() {
            try {
                Thread.sleep(STOP_AFTER_MS);
            } catch (final InterruptedException e) {
                e.printStackTrace();
            }
            stop();
        }
    }
}
