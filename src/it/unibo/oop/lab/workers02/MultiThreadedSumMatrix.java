package it.unibo.oop.lab.workers02;

import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.IntStream;

import org.junit.Assert;

/**
 * Class to calculate the sum of all the elements of a matrix in a multithreaded way.
 *
 */
public class MultiThreadedSumMatrix implements SumMatrix {
    private final int nthread;

    /**
     *
     * @param nthreads
     *         the number of threads to use for the calculation.
     */
    public MultiThreadedSumMatrix(final int nthreads) {
        Assert.assertTrue(nthreads > 0);
        this.nthread = nthreads;
    }

    @Override
    public double sum(final double[][] matrix) {
        Assert.assertTrue(matrix.length > 0);
        Assert.assertTrue(matrix[0].length > 0);
        final int nelems = matrix.length * matrix[0].length;
        final int size = nelems % nthread + nelems / nthread;
        /*
         * Build a list of workers
         */
        final List<Worker> workers = IntStream.iterate(0, start -> start + size)
                .limit(nthread)
                .mapToObj(start -> new Worker(matrix, start, size))
                .collect(Collectors.toList());
        /*
         * Start them
         */
        workers.forEach(Thread::start);
        /*
         * Wait for every one of them to finish
         */
        workers.forEach(t -> {
            try {
                t.join();
            } catch (final InterruptedException e1) {
                e1.printStackTrace();
            }
        });
        /*
         * Return the sum
         */
        return workers.stream().mapToLong(Worker::getResult).sum();
    }


    private static class Worker extends Thread {
        private final double[][] matrix;
        private final int startpos;
        private final int nelem;
        private long res;

        /**
         * Build a new worker.
         *
         * @param matrix
         *            the matrix to sum
         * @param startpos
         *            the initial position for this worker
         * @param nelem
         *            the no. of elems to sum up for this worker
         */
        Worker(final double[][] matrix, final int startpos, final int nelem) {
            super();
            this.matrix = matrix;
            this.startpos = startpos;
            this.nelem = Math.min(nelem, matrix.length * matrix[0].length - startpos);
        }

        @Override
        public void run() {
            System.out.println("Working from position " + startpos + " to position " + (startpos + nelem - 1));
            final int nRows = matrix.length;
            final int nCols = matrix[0].length;

            for (int i = startpos; i < startpos + nelem; i++) {
                final int row = i / nRows;
                final int col = i % nCols;
                this.res += matrix[row][col];
            }
        }

        /**
         * Returns the risult of summing up the integers within the list.
         *
         * @return the sum of every element in the array
         */
        public long getResult() {
            return this.res;
        }

    }
}
